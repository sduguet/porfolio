window.addEventListener('load', function(){
    document.addEventListener('mousemove', parallax);

    function parallax(e){
      this.querySelectorAll('figure div img').forEach(layer => {
        const speed = layer.getAttribute('data-speed');
        const x = (window.innerWidth - e.pageX*speed) / 100;
        const y = (window.innerHeight- e.pageY*speed) / 100;

        layer.style.transform = `translateX(${x}px) translateY(${y}px)`;
      });

      this.querySelectorAll('.ides__illustration img').forEach(layer => {
        const speed = layer.getAttribute('data-speed');
        const x = (window.innerWidth - e.pageX*speed) / 100;
        const y = (window.innerHeight- e.pageY*speed) / 100;

        layer.style.transform = `translateX(${x}px) translateY(${y}px)`;
      });
      
    }

    const btn = document.querySelector('.curve');
    btn.addEventListener('click', function(){
      more();
    });

    const img = document.querySelector('.project--voilovent');
    img.addEventListener('click', function(){
      more()
    });

    function more() {
      const projects = document.querySelector('.projects');
      projects.classList.add('projects--more');
      btn.classList.remove('pointer');
      img.classList.remove('pointer');
    }

    let dynamicStyles = null;
    function addAnimation(body) {
      if (!dynamicStyles) {
        dynamicStyles = document.createElement('style');
        dynamicStyles.type = 'text/css';
        document.head.appendChild(dynamicStyles);
      }

      dynamicStyles.sheet.insertRule(body, dynamicStyles.length);
    }
    
    const projects = document.querySelector('.projects ul');
    addAnimation(`
      @keyframes top { 
        from {
          height: 1100px;
        }
        to {
          height: ${projects.clientHeight + 156}px;
        }
      }
    `);
    
    const desc = document.querySelector('.hero-description');
    elemRect = desc.getBoundingClientRect();
    const lamp = document.querySelector('.lamp');
    lamp.style.left = `${elemRect.left}px`;



    const menu = document.querySelector('#menu');
    const btnsMenu = document.querySelectorAll('.nav__link');
    btnsMenu.forEach(btnMenu => {
      btnMenu.addEventListener('click', function() {
        menu.checked = false;
      });
    });
  });